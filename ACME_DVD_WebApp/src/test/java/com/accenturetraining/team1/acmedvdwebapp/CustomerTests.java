package com.accenturetraining.team1.acmedvdwebapp;

import com.accenturetraining.team1.acmedvdwebapp.model.Customer;
import com.accenturetraining.team1.acmedvdwebapp.services.CustomerServices;
import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@Slf4j
@DataJpaTest
public class CustomerTests {
    @Autowired
    private CustomerServices customerServices;
    
    @Before
    public void dataInitialization() {
    
    }
    
    @Test
    public void testAddCustomer() {
        Customer customer1 = new Customer("Mitsos", "Raptopoulos"
                , "AK359678", "Naxou 31");
        Customer customer2 = new Customer("Nikos", "Nikolopoulos",
                "IB852368", "Limnou 8");
    }
}
