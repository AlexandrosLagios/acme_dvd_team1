package com.accenturetraining.team1.acmedvdwebapp.controllers;

import com.accenturetraining.team1.acmedvdwebapp.dtos.*;
import com.accenturetraining.team1.acmedvdwebapp.model.Customer;
import com.accenturetraining.team1.acmedvdwebapp.services.CustomerServices;
import com.accenturetraining.team1.acmedvdwebapp.services.PaymentServices;
import com.accenturetraining.team1.acmedvdwebapp.services.RentalServices;
import com.accenturetraining.team1.acmedvdwebapp.util.customerExceptions.CustomerAccountIsInactive;
import com.accenturetraining.team1.acmedvdwebapp.util.customerExceptions.CustomerDuplicateException;
import com.accenturetraining.team1.acmedvdwebapp.util.customerExceptions.CustomerInDebtException;
import com.accenturetraining.team1.acmedvdwebapp.util.customerExceptions.RentalsByCustomerNotFound;
import com.accenturetraining.team1.acmedvdwebapp.util.filmExceptions.CopyIsAvailableException;
import com.accenturetraining.team1.acmedvdwebapp.util.filmExceptions.FilmIsUnavailableException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;

@Slf4j
@RestController
@RequestMapping(value = "/customers")
public class CustomerController {
    @Autowired
    CustomerServices customerServices;
    @Autowired
    PaymentServices paymentServices;
    @Autowired
    RentalServices rentalServices;
    
    @GetMapping
    public ResponseEntity<List<CustomerDetailsDTO>> getAllCustomers() {
        List<CustomerDetailsDTO> customers = customerServices.getAllCustomers();
        if (customers.isEmpty()) {
            return new ResponseEntity(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(customers, HttpStatus.OK);
    }
    @GetMapping(value = "/{customerId}")
    public ResponseEntity<?> getCustomerById(@PathVariable long customerId) {
        Customer customer = customerServices.findCustomerById(customerId);
        CustomerDetailsDTO customerDetailsDTO = customerServices.convertCustomerToDTO(customer);
        return new ResponseEntity<>(customerDetailsDTO, HttpStatus.OK);
    }
    
    @PostMapping(produces = "application/json", consumes = "application/json")
    @ResponseBody
    public ResponseEntity<?> registerCustomer(@RequestBody CustomerRequestBodyDTO customerRequestBodyDTO) { // add @Valid
        Customer newCustomer;
        try {
            newCustomer = customerServices.registerCustomer(customerRequestBodyDTO);
            log.info("CUSTOMER REGISTRATION ENDS");
            long customerId = newCustomer.getId();
            String createdMessage = "The customer with id " + customerId + " was created";
            GenericResponseDTO genericResponseDTO = new GenericResponseDTO(customerId, createdMessage);
            return new ResponseEntity<>(genericResponseDTO, HttpStatus.CREATED);
        }
        catch (Exception e) {
            String duplicateCustomerMessage = "A customer with the some of the provided details already " +
                    "exists in the database";
            return new ResponseEntity<>(duplicateCustomerMessage, HttpStatus.BAD_REQUEST);
        }
    }
    @PutMapping(value = "/{customerId}")
    public ResponseEntity<?> updateCustomer(@PathVariable long customerId,
                                            @RequestBody CustomerRequestBodyDTO customerRequestBodyDTO)
            throws CustomerDuplicateException {
        customerServices.editCustomer(customerId, customerRequestBodyDTO);
        String editedMessage = "The customer with id " + customerId + " was edited";
        GenericResponseDTO genericResponseDTO = new GenericResponseDTO(customerId, editedMessage);
        return new ResponseEntity<>(genericResponseDTO, HttpStatus.OK);
    }

    @DeleteMapping(value = "/{customerId}")
    public ResponseEntity<?> deleteCustomer(@PathVariable long customerId) throws CustomerInDebtException {
        Customer customer = customerServices.setCustomerInactive(customerId);
        String deletionMessage = "The customer with the id=" + customerId
                + " was successfully set inactive.";
        GenericResponseDTO genericResponseDTO = new GenericResponseDTO(customerId, deletionMessage);
        return new ResponseEntity<>(genericResponseDTO, HttpStatus.OK);
    }
    
    @GetMapping(value = "/details")
    public ResponseEntity<?> findCustomersByName(@RequestBody CustomerRequestBodyDTO customerRequestBodyDTO) {
        List<CustomerDetailsDTO> customerDetailsDTOS = customerServices.findCustomersByDetails(customerRequestBodyDTO);
        return new ResponseEntity<>(customerDetailsDTOS, HttpStatus.OK);
    }

    @GetMapping(value = "/reports/topcustomers")
    public ResponseEntity<?> getTop20CustomersByPayments()  {
        List<Customer> top20 = customerServices.getTopCustomersByTotalPayments();
        return new ResponseEntity<>(top20, HttpStatus.OK);
    }
    
    //RENTALS
    @GetMapping(value = {"/{customerId}/films", "/{customerId}/rentals"})
    public ResponseEntity<?> getAllCustomerRentals(@PathVariable long customerId)
            throws RentalsByCustomerNotFound {
        customerServices.findCustomerById(customerId);
        List<RentalDetailsDTO> rentalDetailsDTOS = rentalServices.getAllRentalsOfCustomer(customerId);
        return new  ResponseEntity<>(rentalDetailsDTOS, HttpStatus.OK);
    }
    
    @PostMapping(value = "/{customerId}/films/{filmId}/rentals/{copyCategoryId}",
            produces = "application/json")
    public ResponseEntity<?> rentCopy(@PathVariable long customerId, @PathVariable long filmId,
                                      @PathVariable int copyCategoryId) throws FilmIsUnavailableException, CustomerInDebtException, CustomerAccountIsInactive {
        RentalDetailsDTO rentalDetailsDTO = rentalServices.rentCopy(customerId, filmId, copyCategoryId);
        return new  ResponseEntity<>(rentalDetailsDTO, HttpStatus.OK);
    }
    
    @PostMapping(value = "/returns/{copyId}",
            produces = "application/json")
    public ResponseEntity<?> returnCopy(@PathVariable long copyId, @RequestBody String returnDateTime)
            throws CopyIsAvailableException {
        LocalDateTime returnDate = LocalDateTime.parse(returnDateTime);
        RentalReturnDTO rentalReturnDTO = rentalServices.returnCopy(copyId, returnDate);
        return new  ResponseEntity<>(rentalReturnDTO, HttpStatus.OK);
    }
    
    @PostMapping(value = "{customerId}/pay", consumes = MediaType.TEXT_PLAIN_VALUE)
    public ResponseEntity<?> makePayment(@PathVariable long customerId,
                                         @RequestBody String amountText) {
        double amount = Double.parseDouble(amountText);
        PaymentDetailsDTO paymentDetails = paymentServices
                .makePayment(customerId, amount);
        return new ResponseEntity<>(paymentDetails, HttpStatus.CREATED);
    }
}

