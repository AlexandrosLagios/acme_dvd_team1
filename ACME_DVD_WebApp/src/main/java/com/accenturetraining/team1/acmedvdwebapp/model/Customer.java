package com.accenturetraining.team1.acmedvdwebapp.model;


import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.List;

@Data
@NoArgsConstructor
@Entity (name = "customers")
public class Customer {
    
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private long id;
    @Column(name = "is_active")
    private boolean isActive = true;
    @NotNull
    private String firstName;
    @NotNull
    private String lastName;
    @NotNull
    @Column(unique = true)
    private String identificationNumber;
    @NotNull
    private String address;
    @Size(max = 20)
    @NotNull
    @Column(unique = true)
    private String mobileNumber;
    @Size(max = 20)
    private String homeNumber;
    @Size(max = 100)
    private static final String MAIL_PATTERN = "[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{1,63}$";
    @Pattern(regexp = MAIL_PATTERN, message = "Please provide a valid email")
    @Column(unique = true)
    private String email;
    private double debt;
    @Column(name = "total_payments")
    private double totalPayments;
    @JsonIgnore
    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(mappedBy = "customer")
    private List<Rental> rentals;
    @JsonIgnore
    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(mappedBy = "customer")
    private List<Payment> payments;

    public Customer(String identificationNumber, String mobileNumber, String email) {
        this.identificationNumber = identificationNumber;
        this.mobileNumber = mobileNumber;
        this.email = email;
    }
    
    public Customer(String firstName, String lastName, String identificationNumber, String address) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.identificationNumber = identificationNumber;
        this.address = address;
        this.debt = 0.0;
    }
    
    public Customer(String firstName, String lastName, String identificationNumber, String address,
                    String mobileNumber, String email) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.identificationNumber = identificationNumber;
        this.address = address;
        this.mobileNumber = mobileNumber;
        this.email = email;
    }
    
    public Customer(String firstName, String lastName, String address,
                    String homeNumber, String workNumber) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.address = address;
        this.homeNumber = homeNumber;
    }
}
