package com.accenturetraining.team1.acmedvdwebapp.util.filmExceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value= HttpStatus.NOT_FOUND, reason="Copy Not Found") //404
public class CopyNotFoundException extends IndexOutOfBoundsException {
    public CopyNotFoundException(long id) {
        super("CopyNotFoundException with id="+id);
    }
    
    public CopyNotFoundException() {
        super("CopyNotFoundException that was rented during the given period");
    }
}
