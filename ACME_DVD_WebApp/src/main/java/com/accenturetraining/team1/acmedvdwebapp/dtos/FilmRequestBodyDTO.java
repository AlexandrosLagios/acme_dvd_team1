package com.accenturetraining.team1.acmedvdwebapp.dtos;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Data Transfer Object
 * Functions as a request body for register and edit a film
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
public class FilmRequestBodyDTO {
    private String title;
    private String releaseDate;
    private String director;
    private List<ActorDTO> actors;
    private List<String> genres;
    private String description;
}
