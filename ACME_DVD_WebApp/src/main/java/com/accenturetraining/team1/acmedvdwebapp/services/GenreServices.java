package com.accenturetraining.team1.acmedvdwebapp.services;

import com.accenturetraining.team1.acmedvdwebapp.model.Genre;

public interface GenreServices {

    /**
     * adds a new genre
     * @param genreName
     * @return
     */
    Genre registerGenre(String genreName);
}
