package com.accenturetraining.team1.acmedvdwebapp.services;

import com.accenturetraining.team1.acmedvdwebapp.dtos.BetweenDatesDTO;
import com.accenturetraining.team1.acmedvdwebapp.dtos.FilmRentalCountDTO;
import com.accenturetraining.team1.acmedvdwebapp.dtos.RentalDetailsDTO;
import com.accenturetraining.team1.acmedvdwebapp.dtos.RentalReturnDTO;
import com.accenturetraining.team1.acmedvdwebapp.model.Film;
import com.accenturetraining.team1.acmedvdwebapp.model.Rental;
import com.accenturetraining.team1.acmedvdwebapp.util.customerExceptions.CustomerAccountIsInactive;
import com.accenturetraining.team1.acmedvdwebapp.util.customerExceptions.CustomerInDebtException;
import com.accenturetraining.team1.acmedvdwebapp.util.customerExceptions.RentalsByCustomerNotFound;
import com.accenturetraining.team1.acmedvdwebapp.util.filmExceptions.CopyIsAvailableException;
import com.accenturetraining.team1.acmedvdwebapp.util.filmExceptions.FilmIsUnavailableException;

import javax.transaction.Transactional;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

public interface RentalServices {
    /**
     * Checks if the copy was indeed rented, the isRented attribute in the Copy turns false
     * adds the current Date to the returnDate attribute of the corresponding Rental,
     * the total totalCharge is added to the Customer's debt and the Customer is returned.
     * @param customerId
     * @param filmId
     * @param copyCategoryId
     * @return The id of the rental that was completed.
     * @throws FilmIsUnavailableException if all copies of the film of the given copy category
     * are rented
     * @throws CustomerInDebtException if the customer is in debt and can't rent films.
     */
    @Transactional
    RentalDetailsDTO rentCopy(long customerId, long filmId, int copyCategoryId) throws FilmIsUnavailableException, CustomerInDebtException, CustomerAccountIsInactive;
    
//    RentalReturnDTO returnCopy(long copyId) throws CopyIsAvailableException;

    /**
     * Checks if the copy is rented to the particular customer, makes the copy available
     * and calculates the customer debt
     * @param copyId
     * @param returnDateInput
     * @return the customer rental details
     * @throws CopyIsAvailableException
     */
    @Transactional
    RentalReturnDTO returnCopy(long copyId, LocalDateTime returnDateInput) throws CopyIsAvailableException;

    /**
     * Saves a rental to the data base
     * @param rental
     * @return
     */
    Rental updateRental(Rental rental);

    /**
     * Returns a list of rental details for a customer
     * @param id
     * @return
     * @throws RentalsByCustomerNotFound
     */
    List<RentalDetailsDTO> getAllRentalsOfCustomer(long id) throws RentalsByCustomerNotFound;
    
    List<FilmRentalCountDTO> getRentalReportForPeriod(BetweenDatesDTO betweenDatesDTO);
    
    List<FilmRentalCountDTO> convertMapToFilmRentalCountDTO(Map<Film, Integer> filmMap);

    /**
     * prints the report of fil rentals between two dates
     * @param filmRentalCountDTOS
     * @throws IOException
     */
    void printRentalReportToExcel(List<FilmRentalCountDTO> filmRentalCountDTOS) throws IOException;
}
