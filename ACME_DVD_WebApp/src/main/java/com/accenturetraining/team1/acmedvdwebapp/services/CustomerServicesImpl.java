package com.accenturetraining.team1.acmedvdwebapp.services;

import com.accenturetraining.team1.acmedvdwebapp.dtos.CustomerDetailsDTO;
import com.accenturetraining.team1.acmedvdwebapp.dtos.CustomerRequestBodyDTO;
import com.accenturetraining.team1.acmedvdwebapp.model.Customer;
import com.accenturetraining.team1.acmedvdwebapp.model.Payment;
import com.accenturetraining.team1.acmedvdwebapp.model.Rental;
import com.accenturetraining.team1.acmedvdwebapp.repositories.CustomerRepository;
import com.accenturetraining.team1.acmedvdwebapp.repositories.GeneralSettingsRepository;
import com.accenturetraining.team1.acmedvdwebapp.util.customerExceptions.CustomerAccountIsInactive;
import com.accenturetraining.team1.acmedvdwebapp.util.customerExceptions.CustomerDuplicateException;
import com.accenturetraining.team1.acmedvdwebapp.util.customerExceptions.CustomerInDebtException;
import com.accenturetraining.team1.acmedvdwebapp.util.customerExceptions.CustomerNotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
public class CustomerServicesImpl implements CustomerServices {

    @Autowired
    CustomerRepository customerRepository;

    @Autowired
    GeneralSettingsRepository generalSettingsRepository;


    @Override
    public List<CustomerDetailsDTO> getAllCustomers() {
        List<Customer> allCustomers = customerRepository.findAll();
        List<CustomerDetailsDTO> customerDetailsDTOS = new ArrayList<>();
        for (Customer customer : allCustomers) {
            customerDetailsDTOS.add(convertCustomerToDTO(customer));
        }
        log.info(customerDetailsDTOS.toString());
        return customerDetailsDTOS;
    }


    @Override
    public Customer registerCustomer(CustomerRequestBodyDTO customerRequestBodyDTO)
            throws CustomerDuplicateException {
        log.info("STARTING CUSTOMER REGISTRATION");
        Customer customer = new Customer();
        ModelMapper modelMapper = new ModelMapper();
        customer = modelMapper.map(customerRequestBodyDTO, Customer.class);
        log.debug(customer.getIdentificationNumber());
//        validateCustomerForRegistration(customer);
        List<Payment> payments = new ArrayList<>();
        List<Rental> rentals = new ArrayList<>();
        customer.setPayments(payments);
        customer.setRentals(rentals);
        customer.setDebt(0.0);
        customer.setTotalPayments(0.0);
        log.info("CUSTOMER REGISTRATION CONTINUES");
        try {
            return customerRepository.saveAndFlush(customer);
        }
        catch (Exception e) {
            throw new CustomerDuplicateException();
        }
    }


    @Override
    public Customer addCustomer(String firstName, String lastName, String identificationNumber, String address,
                                String mobileNumber, String email) {
        Customer customer = new Customer(firstName, lastName, identificationNumber, address,
                mobileNumber, email);
        customer.setPayments(new ArrayList<>());
        customer.setRentals(new ArrayList<>());
        customer.setDebt(0.0);
        customer.setTotalPayments(0.0);
        return customerRepository.saveAndFlush(customer);
    }


    @Override
    public CustomerDetailsDTO convertCustomerToDTO(Customer customer) {
        CustomerDetailsDTO customerDetailsDTO;
        ModelMapper modelMapper = new ModelMapper();
        customerDetailsDTO = modelMapper.map(customer, CustomerDetailsDTO.class);
        return customerDetailsDTO;
    }


    @Override
    public void validateCustomerDebt(Customer customer) throws CustomerInDebtException {
        if (customer.getDebt() > generalSettingsRepository.getById(1).get().getDebtThreshold()) {
            throw new CustomerInDebtException(customer.getId());
        }
    }


    @Override
    public void checkIfCustomerIsActive(Customer customer) throws CustomerAccountIsInactive {
        if (!customer.isActive()) {
            throw new CustomerAccountIsInactive(customer.getId());
        }
    }


    @Override
    public Customer updateCustomer(Customer customer) throws CustomerDuplicateException {
        try {
            customer = customerRepository.saveAndFlush(customer);
        }
        catch (Exception e) {
            throw new CustomerDuplicateException();
        }
        return customer;
    }

    @Override
    public void editCustomer(long customerId, CustomerRequestBodyDTO customerRequestBodyDTO)
            throws CustomerDuplicateException {
        Customer customer = findCustomerById(customerId);
        customer.setFirstName(customerRequestBodyDTO.getFirstName());
        customer.setLastName(customerRequestBodyDTO.getLastName());
        customer.setAddress(customerRequestBodyDTO.getAddress());
        customer.setIdentificationNumber(customerRequestBodyDTO.getIdentificationNumber());
        customer.setMobileNumber(customerRequestBodyDTO.getMobileNumber());
        customer.setHomeNumber(customerRequestBodyDTO.getHomeNumber());
        updateCustomer(customer);
    }


    @Override
    public List<Customer> getTopCustomersByTotalPayments() {
        List<Customer> top20list = customerRepository.getAllByOrderByTotalPaymentsDesc();
        return top20list;
    }


    @Override
    public Customer setCustomerInactive(long id) throws CustomerInDebtException {
        Customer customer = findCustomerById(id);
        validateCustomerDebt(customer);
        customer.setActive(false);
        return customerRepository.saveAndFlush(customer);
    }


    @Override
    public Customer findCustomerById(long customerId) {
        Customer customer = customerRepository.getById(customerId)
                .orElseThrow(() -> new CustomerNotFoundException(customerId));
        return customer;
    }

    @Override
    public List<CustomerDetailsDTO> findCustomersByDetails(CustomerRequestBodyDTO c) {
        List<CustomerDetailsDTO> customerDetailsDTOS = new ArrayList<>();
        List<Customer> customers =
                customerRepository.findAllByFirstNameContainingAndLastNameContainingAndAddressContainingAndMobileNumberContainingAndHomeNumberContainingAndEmailContainingAndIdentificationNumberContaining
                (c.getFirstName(), c.getLastName(), c.getAddress(),
                        c.getMobileNumber(), c.getHomeNumber(),
                        c.getEmail(), c.getIdentificationNumber())
                        .orElseThrow(() -> new CustomerNotFoundException());
        for (Customer customer : customers) {
            customerDetailsDTOS.add(convertCustomerToDTO(customer));
        }
        
        return customerDetailsDTOS;
    }
    
     // Not working for the moment.
//    @Override
//    public void validateCustomerForRegistration(Customer customer) throws CustomerDuplicateException {
//        log.debug("STARTED CUSTOMER VALIDATION");
//        Customer existingCustomer = customerRepository
//                .getCustomerByIdentificationNumberEquals(customer.getIdentificationNumber())
//                .orElse(null);
//        if (existingCustomer != null) {
//            throw new CustomerDuplicateException
//                    ("identification number", customer.getIdentificationNumber());
//        }
//
//        existingCustomer = customerRepository.getCustomerByMobileNumberEquals(customer.getMobileNumber()).orElse(null);
//        if (existingCustomer != null) {
//            throw new CustomerDuplicateException
//                    ("mobile number", customer.getMobileNumber());
//        }
//
//        existingCustomer = customerRepository.getCustomerByEmailEquals(customer.getEmail()).orElse(null);
//        if(existingCustomer != null) {
//            throw new CustomerDuplicateException
//                    ("email", customer.getEmail());
//        }
//    }
}