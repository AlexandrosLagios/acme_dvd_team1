package com.accenturetraining.team1.acmedvdwebapp.services;

import com.accenturetraining.team1.acmedvdwebapp.dtos.PaymentDetailsDTO;

public interface PaymentServices {
    /**
     * Subtracts the given amount from the debt of the customer with
     * the given id and adds the amount to the customer's total payments.
     * It returns the details of the payment.
     * @param customerId The id of the customer that makes the payment.
     * @param amount The amount paid.
     * @return Returns the details of the payment.
     */
    PaymentDetailsDTO makePayment(long customerId, double amount);
}
