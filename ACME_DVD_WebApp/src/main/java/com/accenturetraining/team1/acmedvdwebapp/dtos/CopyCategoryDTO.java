package com.accenturetraining.team1.acmedvdwebapp.dtos;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CopyCategoryDTO {
    private String categoryName;
    private double initialFee;
    private double dailyLateFee;
    private double firstPenalty;
    private double secondPenalty;
}
