package com.accenturetraining.team1.acmedvdwebapp.repositories;

import com.accenturetraining.team1.acmedvdwebapp.model.CopyCategory;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface CopyCategoryRepository extends JpaRepository<CopyCategory, Integer> {
    Optional<CopyCategory> getById(int id);
    Optional<CopyCategory> getByCategoryName(String categoryName);
}
