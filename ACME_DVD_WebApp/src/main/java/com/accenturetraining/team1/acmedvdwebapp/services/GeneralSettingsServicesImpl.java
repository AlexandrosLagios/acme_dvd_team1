package com.accenturetraining.team1.acmedvdwebapp.services;

import com.accenturetraining.team1.acmedvdwebapp.model.GeneralSettings;
import com.accenturetraining.team1.acmedvdwebapp.repositories.GeneralSettingsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class GeneralSettingsServicesImpl implements GeneralSettingsServices {
    @Autowired
    GeneralSettingsRepository generalSettingsRepository;
    
    @Override
    public int registerNewSettingsProfile(GeneralSettings generalSettings) {
        int id = generalSettingsRepository.save(generalSettings).getId();
        return id;
    }
    
    @Override
    public GeneralSettings updateGeneralSettings(GeneralSettings generalSettings) {
        generalSettingsRepository.save(generalSettings);
        return generalSettings;
    }
    
    @Override
    public GeneralSettings findGeneralSettingsById(int id) {
        return generalSettingsRepository.getById(id).get();
    }
}
