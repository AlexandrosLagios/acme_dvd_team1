package com.accenturetraining.team1.acmedvdwebapp.util.filmExceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value= HttpStatus.BAD_REQUEST, reason="Genre already exists")
public class GenreDuplicateException extends Exception {
    public GenreDuplicateException(String genreName) {
        super("GenreDuplicateException: A genre with the name '" + genreName +
                "' already exists in the database");
    }
}
