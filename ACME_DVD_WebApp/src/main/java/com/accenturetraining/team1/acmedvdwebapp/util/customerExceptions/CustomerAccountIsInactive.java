package com.accenturetraining.team1.acmedvdwebapp.util.customerExceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value= HttpStatus.BAD_REQUEST, reason="Customer Is Inactive")
public class CustomerAccountIsInactive extends Exception {
    public CustomerAccountIsInactive(long customerId) {
        super("CustomerDuplicateException: The customer with the id=" + customerId
                + " is inactive");
    }
}
