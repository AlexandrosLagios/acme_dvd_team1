package com.accenturetraining.team1.acmedvdwebapp.util.filmExceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value= HttpStatus.BAD_REQUEST, reason="Copy category already exists")
public class CopyCategoryDuplicateException extends Exception {
    public CopyCategoryDuplicateException(String categoryName) {
        super("CopyCategoryDuplicateException: A copy category with the name '" + categoryName +
                "' already exists in the database");
    }
}
