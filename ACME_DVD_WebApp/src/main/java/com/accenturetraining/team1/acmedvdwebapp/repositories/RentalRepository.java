package com.accenturetraining.team1.acmedvdwebapp.repositories;

import com.accenturetraining.team1.acmedvdwebapp.model.Rental;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import java.util.Optional;

public interface RentalRepository extends JpaRepository<Rental, Long> {
    List<Rental> findRentalByRentalDate(Date day);
    Optional<Rental> findRentalByCopyIdAndReturnDateIsNull(long copyId);
    Optional<List<Rental>> findAllByCustomerIdOrderByRentalDateDesc(long customerId);
    Optional<List<Rental>> findAllByRentalDateBetween(LocalDate dateFrom, LocalDate dateTo);
}
