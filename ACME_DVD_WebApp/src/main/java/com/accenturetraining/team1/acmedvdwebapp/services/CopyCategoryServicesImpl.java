package com.accenturetraining.team1.acmedvdwebapp.services;

import com.accenturetraining.team1.acmedvdwebapp.dtos.CopyCategoryDTO;
import com.accenturetraining.team1.acmedvdwebapp.model.CopyCategory;
import com.accenturetraining.team1.acmedvdwebapp.repositories.CopyCategoryRepository;
import com.accenturetraining.team1.acmedvdwebapp.util.filmExceptions.CopyCategoryDuplicateException;
import com.accenturetraining.team1.acmedvdwebapp.util.filmExceptions.CopyCategoryNotFoundException;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CopyCategoryServicesImpl implements CopyCategoryServices{
    @Autowired
    CopyCategoryRepository copyCategoryRepository;
    
    @Override
    public long registerCopyCategory(CopyCategoryDTO copyCategoryDTO) throws CopyCategoryDuplicateException {
        validateCopyCategoryForRegistration(copyCategoryDTO);
        ModelMapper modelMapper = new ModelMapper();
        CopyCategory newCopyCategory = modelMapper.map(copyCategoryDTO, CopyCategory.class);
        newCopyCategory = copyCategoryRepository.saveAndFlush(newCopyCategory);
        return newCopyCategory.getId();
    }
    
    @Override
    public CopyCategory findCopyCategoryById(int copyCategoryId) {
        CopyCategory copyCategory = copyCategoryRepository.getById(copyCategoryId)
                .orElseThrow(() -> new CopyCategoryNotFoundException(copyCategoryId));
        return copyCategory;
    }
    
    private void validateCopyCategoryForRegistration(CopyCategoryDTO copyCategoryDTO) throws CopyCategoryDuplicateException {
        List<CopyCategory> copyCategories = copyCategoryRepository.findAll();
        if (copyCategories.stream()
                .anyMatch(copyCategory -> copyCategory.getCategoryName().equals(copyCategoryDTO.getCategoryName()))) {
            throw new CopyCategoryDuplicateException(copyCategoryDTO.getCategoryName());
        }
    }
}
