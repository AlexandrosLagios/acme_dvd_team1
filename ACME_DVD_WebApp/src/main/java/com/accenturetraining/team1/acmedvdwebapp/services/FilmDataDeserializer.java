package com.accenturetraining.team1.acmedvdwebapp.services;

import com.accenturetraining.team1.acmedvdwebapp.dtos.ActorDTO;
import com.accenturetraining.team1.acmedvdwebapp.dtos.FilmJsonDTO;
import com.accenturetraining.team1.acmedvdwebapp.model.Actor;
import com.accenturetraining.team1.acmedvdwebapp.model.Film;
import com.accenturetraining.team1.acmedvdwebapp.model.Genre;
import com.accenturetraining.team1.acmedvdwebapp.repositories.ActorRepository;
import com.accenturetraining.team1.acmedvdwebapp.repositories.GenreRepository;
import com.accenturetraining.team1.acmedvdwebapp.services.FilmServices;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

/**
 * This class is used to deserialize the film data from json
 * Using Data Transfer Objects to map the information from json into our model
 */
@Slf4j
@Service
public class FilmDataDeserializer {
    @Autowired
    FilmServices filmServices;
    @Autowired
    ActorRepository actorRepository;
    @Autowired
    GenreRepository genreRepository;

    
    @Value("classpath:FilmLegacyData.json")
    private Resource filmLegacyDataFile;
    @Transactional
    public List<Film> deserializeFilmDataFromJson() throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        FilmJsonDTO[] filmJsonDTOS = mapper.readValue(filmLegacyDataFile.getFile(), FilmJsonDTO[].class);
        log.info("FILM DTO length : {}", filmJsonDTOS.length);
        List<Film> films = new ArrayList<>();
        for (FilmJsonDTO filmJsonDTO : filmJsonDTOS) {
            Film film = saveFilmFromDTO(filmJsonDTO);
            
            // Add actors to all of the films
            List<Actor> actors = new ArrayList<>();
            film.setActors(actors);
            for (ActorDTO actorDTO : filmJsonDTO.getActor()) {
                Actor actor = saveActorFromJson(actorDTO, film);
                film.getActors().add(actor);
            }
    
            // Add genres to all of the films
            String[] genresNames = filmJsonDTO.getGenre().split("\\|");
            List<Genre> genres = new ArrayList<>();
            film.setGenres(genres);
            for (String genreName : genresNames) {
                Genre genre = saveGenreFromJson(genreName, film);
                film.getGenres().add(genre);
            }
            
            film = filmServices.updateFilm(film);
            films.add(film);
        }
        log.info("FILMS WERE DESERIALIZED");
        return films;
    }
    
    private Film saveFilmFromDTO(FilmJsonDTO filmJsonDTO) {
        Film film = new Film();
        film.setId(filmJsonDTO.getId());
        film.setTitle(filmJsonDTO.getMovieTitle());
        film.setDescription(filmJsonDTO.getDescription());
        film.setDirector(filmJsonDTO.getDirector());
        DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("MM/dd/yyyy");
        LocalDate releaseDate = LocalDate.parse(filmJsonDTO.getYear(), dateFormat);
        film.setReleaseDate(releaseDate);
        Film storedFilm = filmServices.updateFilm(film);
        return storedFilm;
    }
    
    public Actor saveActorFromJson(ActorDTO actorDTO, Film film) {
        String firstName = actorDTO.getFirstName();
        String lastName = actorDTO.getLastName();
        Actor actor = actorRepository.getByFirstNameAndLastName(firstName, lastName)
                .orElse(null);
        if (actor == null) {
            actor = new Actor();
            List<Film> films = new ArrayList<>();
            actor.setFilms(films);
            actor.setFirstName(actorDTO.getFirstName());
            actor.setLastName(actorDTO.getLastName());
        }
        
        actor.getFilms().add(film);
        actor = actorRepository.save(actor);
        return actor;
    }
    
    public Genre saveGenreFromJson(String genreName, Film film) {
        Genre genre = genreRepository.getByGenreName(genreName).orElse(null);
        
        if (genre == null) {
            genre = new Genre();
            List<Film> films = new ArrayList<>();
            genre.setFilms(films);
            genre.setGenreName(genreName);
        }
        genre.getFilms().add(film);
        genre = genreRepository.save(genre);
        return genre;
    }
}
