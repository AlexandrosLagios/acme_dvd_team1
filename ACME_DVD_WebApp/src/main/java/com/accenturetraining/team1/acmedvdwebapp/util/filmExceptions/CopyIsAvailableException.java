package com.accenturetraining.team1.acmedvdwebapp.util.filmExceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value= HttpStatus.BAD_REQUEST, reason="Copy is available")
public class CopyIsAvailableException extends Exception {
    public CopyIsAvailableException(long id) {
        super("CopyIsAvailableExceptionAndCannotBeReturned with id="+id);
    }
}
