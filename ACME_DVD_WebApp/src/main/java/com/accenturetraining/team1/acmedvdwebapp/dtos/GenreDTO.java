package com.accenturetraining.team1.acmedvdwebapp.dtos;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

/**
 * Data Transfer Object
 * Used for mapping genre information
 */

@Data
public class GenreDTO {
    private String genreName;
}
