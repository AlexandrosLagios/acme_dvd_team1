package com.accenturetraining.team1.acmedvdwebapp.services;

import com.accenturetraining.team1.acmedvdwebapp.model.Copy;
import com.accenturetraining.team1.acmedvdwebapp.util.filmExceptions.CopyIsAvailableException;
import com.accenturetraining.team1.acmedvdwebapp.util.filmExceptions.FilmIsUnavailableException;

public interface CopyServices {
    
    /**
     * Adds a new copy of the given title and copyCategory to the database.
     * @param filmId The film of which the copy will be added.
     * @param copyCategoryId The id of the category of the copy.
     * @return the id of the created copy.
     */
    long registerNewCopy(long filmId, int copyCategoryId);

    /**
     * Update Copy
     * @param copy
     * @return
     */
    Copy updateCopy(Copy copy);
    
    /**
     * Checks if a copy with the given id exists and returns it.
     * @param copyId An id.
     * @return The copy with the given id
     */
    Copy findCopyById(long copyId);

    /**
     * Validates if the copy is unavailable to find the out of stock films
     * @param copyId
     * @throws CopyIsAvailableException
     */
    void validateCopyUnavailability(long copyId) throws CopyIsAvailableException;

    /**
     * Checks if a film has available copies in order to rent a copy
     * @param filmId
     * @param copyCategoryId
     * @return copy
     * @throws FilmIsUnavailableException
     */
    Copy checkForAvailableCopyOfFilm(long filmId, int copyCategoryId)
            throws FilmIsUnavailableException;

    /**
     * Checks if a film is available
     * @param filmId
     * @return true if at least one copy is found
     */
    boolean checkForFilmAvailability(long filmId);

    void changeCopyState(long copyId, String state);
}
