package com.accenturetraining.team1.acmedvdwebapp.services;

import com.accenturetraining.team1.acmedvdwebapp.dtos.CopyCategoryDTO;
import com.accenturetraining.team1.acmedvdwebapp.model.CopyCategory;
import com.accenturetraining.team1.acmedvdwebapp.util.filmExceptions.CopyCategoryDuplicateException;

public interface CopyCategoryServices {

    /**
     * Register a copy category
     * @param copyCategoryDTO
     * @return
     */
    long registerCopyCategory(CopyCategoryDTO copyCategoryDTO) throws CopyCategoryDuplicateException;
    
    /**
     * Used for finding the copy category by id
     * @param copyCategoryId
     * @return
     */
    CopyCategory findCopyCategoryById(int copyCategoryId);
}
