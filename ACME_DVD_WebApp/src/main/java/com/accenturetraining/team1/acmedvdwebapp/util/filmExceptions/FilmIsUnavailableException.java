package com.accenturetraining.team1.acmedvdwebapp.util.filmExceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value= HttpStatus.BAD_REQUEST, reason="Film Is Unavailable")
public class FilmIsUnavailableException extends Exception {
    public FilmIsUnavailableException(long id) {
        super("FilmIsUnavailableException with id="+id);
    }
}
