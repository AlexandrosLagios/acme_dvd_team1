package com.accenturetraining.team1.acmedvdwebapp.services;

import com.accenturetraining.team1.acmedvdwebapp.dtos.BetweenDatesDTO;
import com.accenturetraining.team1.acmedvdwebapp.dtos.FilmRentalCountDTO;
import com.accenturetraining.team1.acmedvdwebapp.dtos.RentalDetailsDTO;
import com.accenturetraining.team1.acmedvdwebapp.dtos.RentalReturnDTO;
import com.accenturetraining.team1.acmedvdwebapp.model.*;
import com.accenturetraining.team1.acmedvdwebapp.repositories.CopyRepository;
import com.accenturetraining.team1.acmedvdwebapp.repositories.CustomerRepository;
import com.accenturetraining.team1.acmedvdwebapp.repositories.RentalRepository;
import com.accenturetraining.team1.acmedvdwebapp.util.customerExceptions.CustomerAccountIsInactive;
import com.accenturetraining.team1.acmedvdwebapp.util.customerExceptions.CustomerInDebtException;
import com.accenturetraining.team1.acmedvdwebapp.util.customerExceptions.RentalsByCustomerNotFound;
import com.accenturetraining.team1.acmedvdwebapp.util.filmExceptions.CopyIsAvailableException;
import com.accenturetraining.team1.acmedvdwebapp.util.filmExceptions.CopyNotFoundException;
import com.accenturetraining.team1.acmedvdwebapp.util.filmExceptions.FilmIsUnavailableException;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

import static java.time.temporal.ChronoUnit.DAYS;

@Service
@Slf4j
public class RentalServicesImpl implements RentalServices {
    
    @Autowired
    RentalRepository rentalRepository;
    
    @Autowired
    CopyRepository copyRepository;
    @Autowired
    CustomerRepository customerRepository;
    
    @Autowired
    CopyServices copyServices;
    @Autowired
    CustomerServices customerServices;
    @Autowired
    FilmServices filmServices;
    @Autowired
    GeneralSettingsServices generalSettingsServices;
    
    
    @Transactional
    @Override
    public RentalDetailsDTO rentCopy (long customerId, long filmId, int copyCategoryId) throws CustomerInDebtException, FilmIsUnavailableException, CustomerAccountIsInactive {
        Rental newRental = new Rental();

        // We check that the customer exists.
        Customer customer = customerServices.findCustomerById(customerId);
        customerServices.checkIfCustomerIsActive(customer);
        
        // We check that the film is available
        Copy copy = copyServices.checkForAvailableCopyOfFilm(filmId, copyCategoryId);
        
        // Check if customer is in debt.
        customerServices.validateCustomerDebt(customer);
        
        // The initial fee of the corresponding copyCategory is added to the total charge.
        double initialFee = copy.getCopyCategory().getInitialFee();
        // The new rental is filled with the calculated values and saved to the DB.
        newRental.setTotalCharge(initialFee);
        newRental.setCopy(copy);
        newRental.setCustomer(customer);
        newRental.setRentalDate(LocalDateTime.now());
        newRental = updateRental(newRental);
    
        // Make copy rented.
        copy.setRented(true);
        copy.getRentals().add(newRental);
        copyRepository.save(copy);
        
        RentalDetailsDTO rentalDetailsDTO = convertRentalToDTO(newRental);
        
        // Controller -> 201
        return rentalDetailsDTO;
    }
    
//    @Transactional
//    @Override
//    public RentalReturnDTO returnCopy(long copyId) throws CopyIsAvailableException {
//
//        // Check if copy exists
//        Copy copy = copyServices.findCopyById(copyId);
//
//        // Check if copy is rented to the particular customer
//        copyServices.validateCopyUnavailability(copyId);
//        // Find the corresponding Rental and customer
//        Rental returningRental = rentalRepository.findRentalByCopyIdAndReturnDateIsNull(copyId).get();
//        Customer customer = returningRental.getCustomer();
//
//        copy.setRented(false); // Make copy available.
//        copyRepository.save(copy); // Save copy to db.
//
//
//        LocalDateTime rentalDate = returningRental.getRentalDate();
//        LocalDateTime returnDate = LocalDateTime.now();
//        returningRental.setReturnDate(returnDate); // Set the returnDate of the rental to now.
//        int daysLate = (int) DAYS.between(rentalDate, returnDate);
//
//        // Get the current charge of the rental.
//        double currentCharge = returningRental.getTotalCharge();
//        // Calculate the additional charge.
//        double additionalCharge = calculateAdditionalCharge(daysLate, copy.getCopyCategory(), currentCharge);
//        // Add the additional charge to the current charge and assign the sum to the rental's total charge.
//        double totalCharge = currentCharge + additionalCharge;
//        returningRental.setTotalCharge(totalCharge);
//
//        // Add the charge to the client's debt.
//        double currentDebt = customer.getDebt();
//        customer.setDebt(currentDebt + totalCharge);
//        long customerId = customer.getId();
//        customerRepository.save(customer);
//        updateRental(returningRental);
//        RentalReturnDTO rentalReturnDTO =
//                new RentalReturnDTO(customerId, copyId, daysLate, totalCharge, customer.getDebt());
//        return rentalReturnDTO;
//    }
    
    @Transactional
    @Override
    public RentalReturnDTO returnCopy(long copyId, LocalDateTime returnDateInput) throws CopyIsAvailableException {
        
        // Check if copy exists
        Copy copy = copyServices.findCopyById(copyId);
        
        // Check if copy is rented to the particular customer
        copyServices.validateCopyUnavailability(copyId);
        // Find the corresponding Rental and customer
        Rental returningRental = rentalRepository.findRentalByCopyIdAndReturnDateIsNull(copyId).get();
        Customer customer = returningRental.getCustomer();
        
        copy.setRented(false); // Make copy available.
        copyRepository.save(copy); // Save copy to db.
        
        
        LocalDateTime rentalDate = returningRental.getRentalDate();
        LocalDateTime returnDate = returnDateInput;
        returningRental.setReturnDate(returnDate);
        int daysLate = (int) DAYS.between(rentalDate, returnDate);
        
        // Get the current charge of the rental.
        double currentCharge = returningRental.getTotalCharge();
        // Calculate the additional charge.
        double additionalCharge = calculateAdditionalCharge(daysLate, copy.getCopyCategory(), currentCharge);
        // Add the additional charge to the current charge and assign the sum to the rental's total charge.
        double totalCharge = currentCharge + additionalCharge;
        returningRental.setTotalCharge(totalCharge);
        
        // Add the charge to the client's debt.
        double currentDebt = customer.getDebt();
        customer.setDebt(currentDebt + totalCharge);
        long customerId = customer.getId();
        customerRepository.save(customer);
        updateRental(returningRental);
        RentalReturnDTO rentalReturnDTO =
                new RentalReturnDTO(customerId, copyId, daysLate, totalCharge, customer.getDebt());
        return rentalReturnDTO;
    }
    
    @Override
    public Rental updateRental(Rental rental) {
        rental = rentalRepository.saveAndFlush(rental);
        return rental;
    }
    
    private double calculateAdditionalCharge(int daysLate, CopyCategory copyCategory, double initialFee) {
        GeneralSettings generalSettings = generalSettingsServices.findGeneralSettingsById(1);
        int firstNumberOfDaysLateThreshold = generalSettings.getFirstNumberOfDaysLateThreshold();
        int secondNumberOfDaysLateThreshold = generalSettings.getSecondNumberOfDaysLateThreshold();
        double additionalCharge = 0;
        double dailyLateFee = copyCategory.getDailyLateFee();
        double firstPenalty = copyCategory.getFirstPenalty();
        double secondPenalty = copyCategory.getSecondPenalty();
        // totalCharge = copyCategory.getInitialFee();
        if( daysLate > 1 && daysLate < firstNumberOfDaysLateThreshold){
            additionalCharge = (daysLate - 1) * dailyLateFee;
        }
        else if (daysLate >= firstNumberOfDaysLateThreshold && daysLate < secondNumberOfDaysLateThreshold){
            additionalCharge = firstPenalty
                    + (daysLate-firstNumberOfDaysLateThreshold) * dailyLateFee - initialFee;
        }
        else if (daysLate >= secondNumberOfDaysLateThreshold){
            additionalCharge = secondPenalty
                    + (daysLate-secondNumberOfDaysLateThreshold) * dailyLateFee - initialFee;
        }

        return additionalCharge;
    }
    
    private RentalDetailsDTO convertRentalToDTO (Rental rental) {
        RentalDetailsDTO rentalDetailsDTO = new RentalDetailsDTO();
        rentalDetailsDTO.setRentalId(rental.getId());
        rentalDetailsDTO.setCustomerId(rental.getCustomer().getId());
        rentalDetailsDTO.setRentalDate(rental.getRentalDate());
        rentalDetailsDTO.setReturnDate(rental.getReturnDate());
        rentalDetailsDTO.setFilmId(rental.getCopy().getFilm().getId());
        rentalDetailsDTO.setCopyId(rental.getCopy().getId());
        rentalDetailsDTO.setTotalCharge(rental.getTotalCharge());
        return rentalDetailsDTO;
    }
    
    
    @Override
    public List<RentalDetailsDTO> getAllRentalsOfCustomer(long customerId) throws RentalsByCustomerNotFound {
        customerServices.findCustomerById(customerId);
        
        List<Rental> rentals = rentalRepository
                .findAllByCustomerIdOrderByRentalDateDesc(customerId)
                .orElseThrow(() ->
                        new RentalsByCustomerNotFound(customerId));
        List<RentalDetailsDTO> rentalDetailsDTOS = new ArrayList<>();
        for (Rental rental : rentals) {
            rentalDetailsDTOS.add(convertRentalToDTO(rental));
        }
        return rentalDetailsDTOS;
    }
    @Override
    public List<FilmRentalCountDTO> getRentalReportForPeriod(BetweenDatesDTO betweenDatesDTO){
        DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("MM/dd/yyyy");
        LocalDate dateFrom = LocalDate.parse(betweenDatesDTO.getDateFromText(), dateFormat);
        LocalDate dateTo = LocalDate.parse(betweenDatesDTO.getDateToText(), dateFormat);
        
        Map<Film, Integer> filmMap = new HashMap<>();
        List<Copy> copies = copyRepository.findAllByRentalsNotNull()
                .orElseThrow(() -> new CopyNotFoundException());
        for(Copy copy : copies){
            Film film = copy.getFilm();
            List<Rental> copyRentals = copy.getRentals();
            List<Rental> rentals = copyRentals.stream()
                    .filter(rental -> rental.getRentalDate().isBefore(dateTo.atStartOfDay()))
                    .filter(rental -> rental.getRentalDate().isAfter(dateFrom.atStartOfDay()))
                    .collect(Collectors.toList());
            int copyRentalCount = rentals.size();
            if (copyRentalCount == 0) {
                continue;
            }
            if (filmMap.containsKey(film)) {
                int filmRentalCount = filmMap.get(film);
                filmMap.put(film, filmRentalCount + copyRentalCount);
            }
            else {
                filmMap.put(film, copyRentalCount);
            }
        }
        return convertMapToFilmRentalCountDTO(filmMap);
    }
    
    @Override
    public List<FilmRentalCountDTO> convertMapToFilmRentalCountDTO(Map<Film, Integer> filmMap) {
        List<FilmRentalCountDTO> filmRentalCountDTOS = new ArrayList<>();
        Map<Film, Integer> sortedFilmMap = filmMap.entrySet().stream()
                .sorted(Map.Entry
                        .comparingByValue(Comparator.reverseOrder()))
                .collect(Collectors
                        .toMap(Map.Entry::getKey,
                                Map.Entry::getValue,
                                (oldValue, newValue) ->
                                        oldValue, LinkedHashMap::new));
    
        for (Map.Entry<Film, Integer> sortedFilmMapEntry : sortedFilmMap.entrySet()) {
            FilmRentalCountDTO filmRentalCountDTO = new FilmRentalCountDTO();
            filmRentalCountDTO.setId(sortedFilmMapEntry.getKey().getId());
            filmRentalCountDTO.setTitle(sortedFilmMapEntry.getKey().getTitle());
            filmRentalCountDTO.setReleaseDate(sortedFilmMapEntry.getKey().getReleaseDate());
            filmRentalCountDTO.setRentalCount(sortedFilmMapEntry.getValue());
            filmRentalCountDTOS.add(filmRentalCountDTO);
        }
    
        return filmRentalCountDTOS;
    }
    
    @Override
    public void printRentalReportToExcel(List<FilmRentalCountDTO> filmRentalCountDTOS) throws IOException {
        String[] columns = {"Film Id", "Title", "ReleaseDate", "Rental Count"};
        
        Workbook workbook = new XSSFWorkbook();
        
        CreationHelper createHelper = workbook.getCreationHelper();
    
        Sheet sheet = workbook.createSheet("RentalReport");
        
        log.debug("CREATED SHEET");
        
        Font headerFont = workbook.createFont();
        headerFont.setBold(true);
        headerFont.setFontHeightInPoints((short) 14);
        headerFont.setColor(IndexedColors.DARK_BLUE.getIndex());
    
        CellStyle headerCellStyle = workbook.createCellStyle();
        headerCellStyle.setFont(headerFont);
    
        Row headerRow = sheet.createRow(0);
    
        for(int i = 0; i < columns.length; i++) {
            Cell cell = headerRow.createCell(i);
            cell.setCellValue(columns[i]);
            cell.setCellStyle(headerCellStyle);
        }
    
        log.debug("CREATED HEADERS");
        CellStyle dateCellStyle = workbook.createCellStyle();
        dateCellStyle.setDataFormat(createHelper.createDataFormat().getFormat("dd-MM-yyyy"));
    
        int rowNum = 1;
        for(FilmRentalCountDTO filmRentalCountDTO: filmRentalCountDTOS) {
            Row row = sheet.createRow(rowNum++);
        
            row.createCell(0)
                    .setCellValue(filmRentalCountDTO.getId());
        
            row.createCell(1)
                    .setCellValue(filmRentalCountDTO.getTitle());
        
            Cell dateOfBirthCell = row.createCell(2);
            dateOfBirthCell.setCellValue(filmRentalCountDTO.getReleaseDate().toString());
            dateOfBirthCell.setCellStyle(dateCellStyle);
        
            row.createCell(3)
                    .setCellValue(filmRentalCountDTO.getRentalCount());
        }
        
        log.debug("FILLED ROWS");
        
        for(int i = 0; i < columns.length; i++) {
            sheet.autoSizeColumn(i);
        }
        
        String dateTimeText = new SimpleDateFormat("yyyyMMddHHmm").format(new Date());
        String fileName = "RentalReport-" + dateTimeText + ".xlsx";
        FileOutputStream fileOut = new FileOutputStream(fileName);
        workbook.write(fileOut);
        fileOut.close();
    
        workbook.close();
        log.debug("EXCEL REPORT CREATED");
    }
}
