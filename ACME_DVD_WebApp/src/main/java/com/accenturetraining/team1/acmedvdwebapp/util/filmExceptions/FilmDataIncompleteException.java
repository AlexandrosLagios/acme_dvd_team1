package com.accenturetraining.team1.acmedvdwebapp.util.filmExceptions;

public class FilmDataIncompleteException extends Exception {
    public FilmDataIncompleteException(String field) {
        super("FilmDataIncompleteException, " + field + " must not be null");
    }
}
