package com.accenturetraining.team1.acmedvdwebapp.repositories;

import com.accenturetraining.team1.acmedvdwebapp.model.Genre;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface GenreRepository extends JpaRepository<Genre, Long> {
    Optional<Genre> getByGenreName(String genreName);
    
    @Query(nativeQuery = true, value =
            "DELETE " +
                    "FROM genre_film " +
                    "WHERE genre_id=:genreId " +
                    "AND film_id=:filmId")
    void removeGenreFromFilm(long genreId, long filmId);
}
