package com.accenturetraining.team1.acmedvdwebapp.util.customerExceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value= HttpStatus.NO_CONTENT, reason="Rentals By Customer Not Found")
public class RentalsByCustomerNotFound extends Exception {
    public RentalsByCustomerNotFound(long customerId) {
        super("RentalsByCustomerNotFound for customer with id=" + customerId);
    }
}
