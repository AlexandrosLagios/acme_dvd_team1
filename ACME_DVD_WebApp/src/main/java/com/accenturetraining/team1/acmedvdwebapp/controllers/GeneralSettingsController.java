package com.accenturetraining.team1.acmedvdwebapp.controllers;

import com.accenturetraining.team1.acmedvdwebapp.dtos.FilmRequestBodyDTO;
import com.accenturetraining.team1.acmedvdwebapp.dtos.GenericResponseDTO;
import com.accenturetraining.team1.acmedvdwebapp.model.GeneralSettings;
import com.accenturetraining.team1.acmedvdwebapp.services.GeneralSettingsServices;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@Slf4j
@RequestMapping("/generalSettings")
public class GeneralSettingsController {
    @Autowired
    GeneralSettingsServices generalSettingsServices;

    @PostMapping(produces = "application/json", consumes = "application/json")
    @ResponseBody
    public ResponseEntity<?> registerNewGeneralSettingsProfile(@RequestBody GeneralSettings generalSettings) {
        int id  = generalSettingsServices.registerNewSettingsProfile(generalSettings);
        String createdMessage= "The general settings profile with id=" + id + " was created";
        GenericResponseDTO genericResponseDTO = new GenericResponseDTO(id, createdMessage);
        return new ResponseEntity<>(genericResponseDTO, HttpStatus.CREATED);
    }
}
